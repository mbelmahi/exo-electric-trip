package com.nespresso.exercise.electric_trip;

/**
 * Created by Mohamed BELMAHI on 12/08/2016.
 */
public class Location {
    LocationEnum name;
    Integer chargedValuePerHour;

    public Location(LocationEnum name, Integer chargedValuePerHour) {

        this.name = name;
        this.chargedValuePerHour = chargedValuePerHour;
    }

    public LocationEnum name() {
        return name;
    }
}
