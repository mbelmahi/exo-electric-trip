package com.nespresso.exercise.electric_trip;

/**
 * Created by Mohamed BELMAHI on 12/08/2016.
 */
public class Trip {
    private final Location starting;
    private final Integer distance;
    private final Location destination;

    public Trip(Location starting, Integer distance, Location destination) {

        this.starting = starting;
        this.distance = distance;
        this.destination = destination;
    }

    public LocationEnum newLocation(Integer maxAchievableDistance) {
        return maxAchievableDistance >= distance ? destination.name() : starting.name();
    }

    public int batterySizeAfterTraveling(int batterySize, int lowSpeedPerformance) {
        return batterySize - (distance / lowSpeedPerformance);
    }
}
