package com.nespresso.exercise.electric_trip;

/**
 * Created by Mohamed BELMAHI on 12/08/2016.
 */
public enum LocationEnum {
    PARIS, LIMOGES, BORDEAUX, BOURGES, MARSEILLES
}
