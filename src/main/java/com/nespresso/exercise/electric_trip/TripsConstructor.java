package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Mohamed BELMAHI on 12/08/2016.
 */
class TripsConstructor {
    static Map<LocationEnum,Trip> construct(String travelInput) {
        HashMap<LocationEnum, Trip> trips = new LinkedHashMap<LocationEnum, Trip>();

        String[] tripsInfos = travelInput.split("-");

        int inputBegin = 0;
        int inputEnd = 2;
        while (inputEnd < tripsInfos.length){

            String starting = InputTripParser.parseStarting(travelInput, inputBegin);
            Integer distance = InputTripParser.parseDistance(travelInput, inputBegin);
            String destination = InputTripParser.parseDestination(travelInput, inputBegin);


            trips.put(LocationEnum.valueOf(starting), constructTrip(starting, destination, distance));

            inputBegin += 2;
            inputEnd += 2;
        }

        return trips;
    }

    private static Trip constructTrip(String starting, String destination, Integer distance){
        String[] startingInfo = starting.split(":");
        String[] destinationInfo = destination.split(":");

        LocationEnum startLocationName = LocationEnum.valueOf(startingInfo[0]);
        Integer startChargedValuePerHour = startingInfo.length > 1 ? Integer.valueOf(startingInfo[1]) : 0;
        Location startingLocation = new Location(startLocationName, startChargedValuePerHour);

        LocationEnum destLocationName = LocationEnum.valueOf(destinationInfo[0]);
        Integer destChargedValuePerHour = destinationInfo.length > 1 ? Integer.valueOf(destinationInfo[1]) : 0;

        Location destinationLocation = new Location(destLocationName, destChargedValuePerHour);
        Trip trip = new Trip(startingLocation, distance, destinationLocation);

        return trip;
    }

    static class InputTripParser {

        private static final int STARTING_INDEX = 0;
        private static final int DISTANCE_INDEX = 1;
        private static final int DESTINATION_INDEX = 2;

        static String parseStarting(String travelInput, int inputBegin) {
            String[] travelInfos = travelInput.split("-");
            return travelInfos[inputBegin + STARTING_INDEX];
        }

        static Integer parseDistance(String travelInput, int inputBegin) {
            String[] travelInfos = travelInput.split("-");
            return Integer.valueOf(travelInfos[inputBegin + DISTANCE_INDEX]);
        }

        static String parseDestination(String travelInput, int inputBegin) {
            String[] travelInfos = travelInput.split("-");
            return travelInfos[inputBegin + DESTINATION_INDEX];
        }
    }
}
