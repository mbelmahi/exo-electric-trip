package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Mohamed BELMAHI on 12/08/2016.
 */
public class ElectricTrip {

    private Map<LocationEnum, Trip> trips = new LinkedHashMap<LocationEnum, Trip>();
    private Map<Integer, Car> travelingCars = new HashMap<Integer, Car>();
    private Integer generatedId = 0;

    public ElectricTrip(String travelInput) {

        this.trips = TripsConstructor.construct(travelInput);
    }

    public int startTripIn(String starting, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
        Car car = new Car(starting, trips, batterySize, lowSpeedPerformance, highSpeedPerformance);

        int carId = generatedId;
        generatedId++;

        travelingCars.put(carId, car);
        return carId;
    }

    public void go(int participantId) {
        travelingCars.get(participantId).go();
    }

    public String locationOf(int participantId) {
        return travelingCars.get(participantId).location();
    }

    public String chargeOf(int participantId) {
        return travelingCars.get(participantId).charge();
    }

    public void sprint(int participantId) {
        travelingCars.get(participantId).sprint();
    }

    public void charge(int id, int hoursOfCharge) {
    }
}
