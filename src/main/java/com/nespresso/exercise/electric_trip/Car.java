package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mohamed BELMAHI on 12/08/2016. new
 */
public class Car {
    private final Map<LocationEnum, Trip> trips;
    private final int initBatterySize;
    private final int lowSpeedPerformance;
    private int highSpeedPerformance;
    private int batterySize;
    private LocationEnum location;

    public Car(String starting, Map<LocationEnum, Trip> trips, int batterySize,
               int lowSpeedPerformance, int highSpeedPerformance) {

        this.trips = trips;
        this.initBatterySize = batterySize;
        this.batterySize = batterySize;
        this.lowSpeedPerformance = lowSpeedPerformance;
        this.highSpeedPerformance = highSpeedPerformance;
        this.location = LocationEnum.valueOf(starting);
    }

    public String location() {
        return location.toString();
    }

    public String charge() {
        double percentCharge = ((double) batterySize / (double) initBatterySize);
        percentCharge *= 100;
        String charge = Math.round(percentCharge) + "%";
        return charge;
    }

    public void go() {
        traveling(lowSpeedPerformance);
    }

    public void sprint() {
        traveling(highSpeedPerformance);
    }

    private void traveling(int speedPerformance) {
        LocationEnum startingLocation = location;
        for (Trip trip : SubTripFromToEnd(startingLocation)) {
            Integer maxAchievableDistance = speedPerformance * batterySize;
            location = trip.newLocation(maxAchievableDistance);
            if (startingLocation.equals(location)) {
                break;
            }
            startingLocation = location;
            batterySize = trip.batterySizeAfterTraveling(batterySize, speedPerformance);
        }
    }

    private List<Trip> SubTripFromToEnd(LocationEnum startingLocation) {
        List<Trip> subTrips = new ArrayList<Trip>();
        boolean startAddToSubList = false;
        for (Map.Entry<LocationEnum, Trip> tripEntry : this.trips.entrySet()) {
            startAddToSubList = startAddToSubList || tripEntry.getKey().equals(startingLocation);
            if (startAddToSubList) {
                subTrips.add(tripEntry.getValue());
            }
        }

        return subTrips;
    }
}
